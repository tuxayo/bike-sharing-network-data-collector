import unittest

from bsn_data_collector.extract import extract_from_API
from bsn_data_collector.extract import ApiError
from test.test_helpers.mocker_API_request import MockerApiRequest


class Test(unittest.TestCase):

    """
    Check that the mocker works.
    """
    def test_handle_API_correct_response_mocked(self):
        mocker = MockerApiRequest()
        mocker.mock_network_query()
        try:
            stations, location = extract_from_API()
            self.assertEqual('Sevilla Mocked', location['city'])
            self.assertEqual([], stations)
        finally:
            mocker.remove_network_query_mock()

    def test_handle_API_response_without_stations(self):
        mocker = MockerApiRequest(response_content=MockerApiRequest.RESPONSE_WITHOUT_STATIONS)
        mocker.mock_network_query()
        try:
            with self.assertRaises(ApiError):
                stations, location = extract_from_API()
        finally:
            mocker.remove_network_query_mock()

    def test_handle_API_response_code_not_200(self):
        mocker = MockerApiRequest(status_code=418)
        mocker.mock_network_query()
        try:
            with self.assertRaises(ApiError):
                stations, location = extract_from_API()
        finally:
            mocker.remove_network_query_mock()

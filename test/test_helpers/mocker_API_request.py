import requests


# Uses monkey patching
class MockerApiRequest:
    RESPONSE_CORRECT_MINIMAL = {  # empty station list
        'network': {
            'location': {
                'city': 'Sevilla Mocked',
                'country': 'ES',
                'longitude': -5.9823299,
                'latitude': 37.3880961},
            'stations': []}}

    RESPONSE_WITHOUT_STATIONS = {
        'network': {
            'location': {
                'city': 'Sevilla Mocked',
                'country': 'ES',
                'longitude': -5.9823299,
                'latitude': 37.3880961}}}

    def __init__(self,
                 status_code=200,
                 response_content=RESPONSE_CORRECT_MINIMAL):
        self.status_code = status_code
        self.response_content = response_content

    def mock_network_query(self):
        def get_method_mock(_):
            return ResponseMock(status_code=self.status_code,
                                json=lambda: self.response_content,
                                text=str(self.response_content))
        print('mock_network_query')
        setattr(requests, '_get_backup', requests.get)
        requests._get_backup = requests.get
        requests.get = get_method_mock

    def remove_network_query_mock(self):
        print('remove_network_query_mock')
        requests.get = requests._get_backup


# Here we create a dict whose keys can be accessed using the dot notation
# Making it a completely dynamic object with attributes and methods
class ResponseMock(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

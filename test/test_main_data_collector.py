import unittest

from bsn_data_collector.main_data_collector import ETL

from test.test_helpers.mocker_API_request import MockerApiRequest


class Test(unittest.TestCase):
    def test_ETL_should_handle_empty_API_response(self):
        mocker = MockerApiRequest(response_content=MockerApiRequest.RESPONSE_WITHOUT_STATIONS)
        mocker.mock_network_query()
        try:
            ETL()
        finally:
            mocker.remove_network_query_mock()

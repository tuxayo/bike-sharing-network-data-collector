import unittest

from bsn_data_collector.transform.transform import _convert_stations_timestamps_to_local_time
from bsn_data_collector.transform.transform import transform


class Test(unittest.TestCase):
    def test_convert_station_should_ignore_ambigous_timestamps(self):
        location = {'longitude': -5.9823299,
                    'latitude': 37.3880961,
                    'city': 'Sevilla',
                    'country': 'ES'}
        # only relevant data for the test was kept in the following sample
        stations_timestamps = [{'timestamp': '2016-11-25T06:49:41.572000Z'},
                               {'timestamp': '2016-10-30T02:30:00.037000Z'},
                               {'timestamp': '2016-11-25T06:49:41.721000Z'}]
        result = _convert_stations_timestamps_to_local_time(
            stations_timestamps, location)
        self.assertEqual(2, len(list(result)))

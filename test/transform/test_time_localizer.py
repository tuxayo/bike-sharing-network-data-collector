import unittest
import copy

import pytz

from bsn_data_collector.transform.time_localizer_singleton import TimeLocalizerSingleton


class Test(unittest.TestCase):
    time_localizer = None

    @classmethod
    def setUpClass(cls):
        sevilla_coordinates = {
            "latitude": 37.397829929383,
            "longitude": -5.97567172039552
        }
        cls.time_localizer = TimeLocalizerSingleton(sevilla_coordinates)

    def test_timezone_should_be_madrid(self):
        self.assertEqual("Europe/Madrid",
                         self.time_localizer.timezone.zone)

    def test_localize_time_in_summer(self):
        datetime_str_to_localize = "2016-06-01T15:16:40.037000Z"
        localized_date = self.time_localizer.localize(
            datetime_str_to_localize)
        self.assertEqual(17, localized_date.hour)

    def test_localize_time_in_winter(self):
        datetime_str_to_localize = "2016-11-01T15:16:40.037000Z"
        localized_date = self.time_localizer.localize(
            datetime_str_to_localize)
        self.assertEqual(16, localized_date.hour)

    def test_localize_ambigous_time_during_time_change(self):
        datetime_str_to_localize = "2016-10-30T02:30:00.037000Z"
        with self.assertRaises(pytz.exceptions.AmbiguousTimeError):
            self.time_localizer.localize(datetime_str_to_localize)

    def test_localize_negative_UTC_offset(self):
        # copy and set timezone directly to avoid using TimeLocalizer()
        # because finding a timezone from coordinates is slow
        chile_time_localizer = copy.copy(self.time_localizer)
        chile_time_localizer.timezone = pytz.timezone("America/Santiago")

        datetime_str_to_localize = "2016-08-01T15:16:40.037000Z"
        # in october, Chile uses summer time
        localized_date = chile_time_localizer.localize(
            datetime_str_to_localize)
        self.assertEqual(11, localized_date.hour)


if __name__ == "__main__":
    unittest.main()

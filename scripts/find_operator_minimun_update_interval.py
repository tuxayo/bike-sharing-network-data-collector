#!/usr/bin/env python2

# Context:
# I wanted to know if the operator on some bike sharing network was
# updating faster it's API than CityBikes was quering it.

# Description:
# Script that query a station at a given rate (it must be small enough) to
# find the mininum time interval at which an operator update it's API.
# (from the point of view of CityBikes)
# It displays the delta and if you wait enough you will be able to make
# a guess of the minimun update rate of CityBikes.

# Usage:
# clone pybikes https://github.com/eskerda/pybikes/
# put this script in pybikes directory
# run it

from __future__ import print_function
import time
import sys

import pybikes  # https://github.com/eskerda/pybikes/

RATE = 5  # seconds

print('SELECT YOUR NETWORK AND PUT YOUR API KEY IF NECESSARY (go find this print in the code)')
# sev = pybikes.get('sevici', 'API KEY GOES HERE IF NEEDED') # uncomment me

STATION_NAME_SUBSTRING = 'ARMAS'  # try to choose a popular one


def main():
    if len(sys.argv) < 2:
        sys.exit('Usage: `%s station_name` (or part of a station name),'
                 '\ntry to choose a popular one' % sys.argv[0])
    station_name_substring = sys.argv[1]
    previous_update_time = 0
    sev.update()
    station = find_first_station_whose_name_contains(station_name_substring)
    print('name: ' + station.name)
    while True:
        previous_update_time = update(previous_update_time,
                                      station_name_substring)
        time.sleep(RATE)


def update(previous_update_time, station_name_substring):
    sev.update()
    station = find_first_station_whose_name_contains(station_name_substring)
    last_update = station.extra['last_update']
    time_delta = (last_update - previous_update_time)/1000  # timestamps in ms
    if time_delta == 0:
        print('.', end='')
        sys.stdout.flush()
    else:
        print()
        print('last update: ' + str(last_update) +
              ' - bikes: ' + str(station.bikes) +
              ' - slots: ' + str(station.free) +
              ' - time delta with previous update: ' + str(time_delta))
    return last_update


def find_first_station_whose_name_contains(query):
    return next(station for station in sev.stations if query in station.name)

if __name__ == '__main__':
    main()

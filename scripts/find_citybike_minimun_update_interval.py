#!/usr/bin/env python3

# Context:
# I wanted to know if the operator on some bike sharing network was
# updating faster it's API than CityBikes was quering it.

# Description:
# Script that query a station at a given rate (it must be small enough) to
# find the mininum time interval at which an operator update it's API.
# It displays the delta and if you wait enough you will be able to make
# a guess of the minimun update rate (if any)


import time
import sys

import requests

RATE = 10  # seconds
STATION_NAME_SUBSTRING = 'ARMAS'  # try to choose a popular one

NETWORK_API_URL = 'http://api.citybik.es/v2/networks/sevici'


def main():
    if len(sys.argv) < 2:
        sys.exit('Usage: `%s station_name` (or part of a station name),'
                 '\ntry to choose a popular one' % sys.argv[0])
    station_name_substring = sys.argv[1]
    previous_update_time = 0
    response = requests.get(NETWORK_API_URL)
    station = find_first_station_whose_name_contains(station_name_substring,
                                                     response)
    print('name: ' + station['name'])
    while True:
        previous_update_time = update(previous_update_time,
                                      station_name_substring)
        time.sleep(RATE)


def update(previous_update_time, station_name_substring):
    response = requests.get(NETWORK_API_URL)
    station = find_first_station_whose_name_contains(station_name_substring,
                                                     response)
    last_update = station['extra']['last_update']
    time_delta = (last_update - previous_update_time)/1000  # timestamps in ms
    if time_delta == 0:
        print('.', end='')
        sys.stdout.flush()
    else:
        print()
        print('last update: ' + str(last_update) +
              ' - bikes: ' + str(station['free_bikes']) +
              ' - time delta with previous update: ' + str(time_delta))
    return last_update


def find_first_station_whose_name_contains(query, response):
    stations = response.json()['network']['stations']
    return next(station for station in stations if query in station['name'])

if __name__ == '__main__':
    main()

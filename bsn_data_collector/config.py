import configparser
import os


# load config file
_this_module_dir = os.path.dirname(__file__)
_config_file_path = os.path.join(_this_module_dir, '..', 'config.ini')
_config_file = configparser.ConfigParser()
_config_file.read(_config_file_path)

NETWORK_API_URL = _config_file['BSNDC']['NETWORK_API_URL']
QUERY_INTERVAL = int(_config_file['BSNDC']['QUERY_INTERVAL'])

DATA_FOLDER = os.path.join(_this_module_dir, '..', 'data')
DB_FILE_PATH = os.path.join(DATA_FOLDER, _config_file['BSNDC']['DB_FILE'])

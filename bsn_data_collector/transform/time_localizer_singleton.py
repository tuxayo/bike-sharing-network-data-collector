# Metaclass singleton
# https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python

from bsn_data_collector.transform.time_localizer import TimeLocalizer


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            print("Initializing TimeLocalizer, it might take from few seconds to one minute depending on your CPU and happen only once")
            cls._instances[cls] = super(Singleton, cls).__call__(*args,
                                                                 **kwargs)
            print("TimeLocalizer ready")
        return cls._instances[cls]


class TimeLocalizerSingleton(TimeLocalizer, metaclass=Singleton):
    pass

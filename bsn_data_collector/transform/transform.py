import pytz

from bsn_data_collector.transform.time_localizer_singleton import TimeLocalizerSingleton


def transform(stations, location):
        stations_usage_data = _keep_dynamic_usage_data(stations)
        stations_static_data = _keep_static_data(stations)
        stations_usage_data = _convert_stations_timestamps_to_local_time(
            stations_usage_data, location)
        return (stations_usage_data, stations_static_data)


def _keep_dynamic_usage_data(stations):
    """
    Keeps only the dynamic usage data, so it will be stored in it's own table
    to avoid data duplication. (Store the static data only once)
    """
    def prune_station(station):
        return {
            'id': station['id'],
            'free_bikes': station['free_bikes'],
            'empty_slots': station['empty_slots'],
            'timestamp': station['timestamp']
        }
    return [prune_station(station) for station in stations]


def _keep_static_data(stations):
    def prune_station(station):
        return {
            'id': station['id'],
            'name': station['name'],
            'address': station['extra']['address'],
            'latitude': station['latitude'],
            'longitude': station['longitude']
        }
    return [prune_station(station) for station in stations]


def _convert_stations_timestamps_to_local_time(stations, location):
    time_localizer = TimeLocalizerSingleton(location)

    def convert_station_timestamp_to_local_time(station):
        try:
            new_timestamp = time_localizer.localize(station['timestamp'])
            station['timestamp'] = new_timestamp
            return station
        except pytz.exceptions.AmbiguousTimeError:
            return None
    stations = map(convert_station_timestamp_to_local_time, stations)
    return filter(None, stations)

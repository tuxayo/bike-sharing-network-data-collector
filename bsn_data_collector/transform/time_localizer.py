import dateutil.parser

import pytz
from tzwhere import tzwhere


class TimeLocalizer:

    def __init__(self, location):
        tzw = tzwhere.tzwhere()
        timezone_str = tzw.tzNameAt(location["latitude"],
                                    location["longitude"])
        self.timezone = pytz.timezone(timezone_str)

    def localize(self, date_string_iso_8601):
        # pytz needs a naive datetime (without timezone)
        utc_datetime = dateutil.parser.parse(date_string_iso_8601,
                                             ignoretz=True)
        utc_offset = self.timezone.utcoffset(utc_datetime)
        return utc_datetime + utc_offset

import time

import bsn_data_collector.config as config
from bsn_data_collector.extract import extract_from_API
from bsn_data_collector.extract import ApiError
from bsn_data_collector.transform import transform
from bsn_data_collector.load import load_into_DB
from bsn_data_collector.load import setup_DB


def data_collector():
    print('Initializing database')
    setup_DB()
    print('Data collection started')
    while True:
        ETL()
        print('.', end='', flush=True)
        # sleeping doesn't take into account the time to perform the ETL
        # (network + processing) but it shouldn't be a problem
        time.sleep(config.QUERY_INTERVAL)


def ETL():
    """
    https://en.wikipedia.org/wiki/Extract,_transform,_load
    """
    # extract
    try:
        stations, location = extract_from_API()
    except ApiError as e:
        print(e)
        return
    # transform
    usage_data, stations_static_data = transform(stations, location)
    # load
    load_into_DB(usage_data, stations_static_data)


def main():
    data_collector()


if __name__ == '__main__':
    main()

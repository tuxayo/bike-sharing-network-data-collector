import sqlite3

import bsn_data_collector.config as config


def setup_DB():
    db = sqlite3.connect(config.DB_FILE_PATH)
    cursor = db.cursor()
    CREATE_HISTORY_TABLE = '''
        CREATE TABLE IF NOT EXISTS stations_history(
        id VARCHAR(40),
        timestamp VARCHAR(30),
        empty_slots INTEGER,
        free_bikes INTEGER
        )'''
    CREATE_STATIC_DATA_TABLE = '''
        CREATE TABLE IF NOT EXISTS stations_static_data(
        id VARCHAR(40),
        name VARCHAR(70),
        address VARCHAR(120),
        latitude REAL,
        longitude REAL
        )'''
    cursor.execute(CREATE_HISTORY_TABLE)
    cursor.execute(CREATE_STATIC_DATA_TABLE)
    db.close()


def load_into_DB(stations_snapshot, static_data):
    db = sqlite3.connect(config.DB_FILE_PATH)
    cursor = db.cursor()

    replace_static_data(cursor, static_data)

    INSERT_STATION_USAGE_DATA = '''
        INSERT INTO stations_history
        VALUES (:id, :timestamp, :empty_slots , :free_bikes)'''
    cursor.executemany(INSERT_STATION_USAGE_DATA,
                       stations_snapshot)
    db.commit()
    db.close()


def replace_static_data(db_cursor, static_data):
    """
    Replacing static data each time is by far the easiest way to keep it up
    to date. If it's a practical problem then a more complex but efficient
    solution shall be found.
    """
    DELETE_STATIC_DATA = 'DELETE FROM stations_static_data'
    db_cursor.execute(DELETE_STATIC_DATA)
    INSERT_STATIC_DATA = '''
        INSERT INTO stations_static_data
        VALUES (:id, :name, :address, :latitude, :longitude)'''
    db_cursor.executemany(INSERT_STATIC_DATA,
                          static_data)

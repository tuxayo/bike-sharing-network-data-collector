import requests

import bsn_data_collector.config as config
from bsn_data_collector.extract import ApiError


def extract_from_API():
    try:
        response = requests.get(config.NETWORK_API_URL)
    except requests.exceptions.ConnectionError as e:
        raise ApiError(e) from e
        return
    if response.status_code != 200:
        raise ApiError('Something went wrong with the request '
                       'to the API. Status code: '
                       + str(response.status_code)
                       + ' response content: \n'
                       + response.text)
    try:
        stations = response.json()['network']['stations']
        location = response.json()['network']['location']
        return (stations, location)
    except KeyError as e:
        message = ('for some reason, a part of the response is '
                   'missing: ' + e.args[0])
        raise ApiError(message) from e
